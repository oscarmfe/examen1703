<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = ['id', 'title', 'pages', 'year', 'gender_id', 'user_id'];

    public function authors()
    {
        return $this->belongsToMany('App\Author');
    }

    public function genders()
    {
        return $this->belongsTo('App\Gender');
    }
}
    