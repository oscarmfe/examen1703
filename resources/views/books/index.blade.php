@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Libros</div>

                <div class="panel-body">
                    <a href="/books/create">Crear un libro</a>
                    <table class="table">
                        <thead>
                            <th>Usuario</th>
                            <th>Título</th>
                            <th>Género</th>
                            <th>Año</th>
                       
                        </thead>
                        @foreach ($books as $book)
                            <tr>
                                <td>{{ $book->user_id }}</td>
                                <td>{{ $book->title }}</td>
                                <td>{{ $book->gender_id }}</td>
                                <td>{{ $book->year }}</td>                               
                                <td>
                         <form method="post" action="/books/{{ $book->id }}">
                        <input type="hidden" name="_method" value="DELETE">
                        {{ csrf_field() }}
                        <input type="submit" value="Borrar">
                        <a href="/books/{{ $book->id }}/show">Ver</a>
                    </td>
                </tr>
                        @endforeach
                    </table>
                    {{ $books->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
