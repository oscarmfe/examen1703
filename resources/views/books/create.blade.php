@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Libros</div>

                <div class="panel-body">
                    <p>Creación de un libro</p>
                    <div class="form">
                    <form  action="/books" method="post">
                    {{ csrf_field() }}


                    <div class="form-group">
                        <label>Nombre: </label>
                        <input type="text" name="name" value="{{ old('name') }}">
                        {{ $errors->first('name') }}
                    </div>
                    <div class="form-group">
                        <label>Género: </label>
                        <select class="form-control" name="type_id">
                            @foreach ($genders as $gender)
                                <option value="{{$gender->id}}">{{$gender->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Titulo: </label>
                        <input type="text" name="description" value="{{ old('description') }}">
                        {{ $errors->first('description') }}
                    </div>
                    <input type="submit" value="Guardar">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
